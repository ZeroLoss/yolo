import sys

filename = sys.argv[1]

f = open(filename, "r")
numImage = 0
numPos = 0

for x in f:
  numImage +=1
  if x != "[]\n" and x != "[]":
    numPos += 1

print "Number of Images analysed " + str(numImage)
print "Number of Positively Classified Images " + str(numPos)
