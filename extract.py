import sys
from PIL import Image  # uses pillow

filename = sys.argv[1]
imagedir = sys.argv[2]
f = open(filename, "r")
for x in f:
  parts = x.split()
  imageFile = parts[0].split("/")
  imageName = imageFile[-1].split(".")

  im = Image.open(imagedir + imageFile[-1])
  imSize = im.size
  imwidth = imSize[0]
  imheight = imSize[1]

  newName = imageName[0] + ".txt"
  f2 = open(newName, "w")
  count = 0
  start = "0 "
  line = ""
  xpos = 0
  ypos = 0
  w = 0
  h = 0

  for y in parts[2:]:
    count +=1
    if count == 1:
      xpos = y
    elif count == 2:
      ypos = y
    elif count == 3:
      w = y
    else:
      h = y
      f2.write(start)
      f2.write(str(((float(xpos) + (float(w)/2))/float(imwidth))) + " ")
      f2.write(str(((float(ypos) + (float(h)/2))/float(imheight))) + " ")
      f2.write(str((float(w)/float(imwidth))) + " ")
      f2.write(str((float(h)/float(imheight))) + " ")
      f2.write("\n")
      count = 0


#  for y in parts[2:]:
#     if count == 0:
#       f2.write(start)
#     if count < 4:
#       temp = count % 2
#       if temp == 0:
#         f2.write(str((float(y)/float(imwidth))) + " ")
#       else:
#         f2.write(str((float(y)/float(imheight))) + " ")
#       count +=1
#     else:
#       f2.write("\n")
#       f2.write(start)
#       f2.write(str((float(y)/float(imwidth))) + " ")
#       count = 1
